import { icnEmail, icnGitlab, icnGitlabBlack, icnLinkedin, icnSkype } from '../assets/icons/icons';

const map = new Map<string, string>([
    ['linkedin', icnLinkedin],
    ['email', icnEmail],
    ['gitlab', icnGitlab],
    ['gitlabBlack', icnGitlabBlack],
    ['skype', icnSkype],
]);

export const contactMap = {
    get: (key: string) => map.get(key) ?? ''
};
