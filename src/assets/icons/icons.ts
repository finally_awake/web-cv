import linkedin from './iconmonstr-linkedin-2.svg'
import user from './iconmonstr-user-1.svg'
import users from './iconmonstr-user-22.svg'
import skype from './iconmonstr-skype-1.svg'
import email from './iconmonstr-email-4.svg'
import gitlab from './gitlab-icon-rgb.svg'
import gitlabBlack from './gitlab-icon-1-color-black-rgb.svg'
import location from './iconmonstr-location-1.svg'
import calendar from './iconmonstr-calendar-4.svg'
import earth from './iconmonstr-globe-5.svg'

import angular from './technologies/angularjs-plain.svg'


export const icnLinkedin = linkedin;
export const icnUser = user;
export const icnUsers = users;
export const icnSkype = skype;
export const icnEmail = email;
export const icnGitlab = gitlab;
export const icnGitlabBlack = gitlabBlack;
export const icnLocation = location;
export const icnCalendar = calendar;
export const icnEarth = earth;
export const icnAngular = angular;

