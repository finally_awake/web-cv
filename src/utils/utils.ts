import { Dayjs } from 'dayjs';

export function getTimespanString(from: Dayjs, to: Dayjs): string {
    const yearsDiff = to.diff(from, 'year');
    const monthsDiff = to.diff(from, 'month') - yearsDiff*12;

    let result = ''

    if (yearsDiff !== 0){
        result += `${yearsDiff} `;

        result += yearsDiff === 1? 'year ' : 'years ';
    }

    if (monthsDiff !== 0){
        result += `${monthsDiff} `;

        result += monthsDiff === 1? 'month' : 'months';
    }

    return result;
}

