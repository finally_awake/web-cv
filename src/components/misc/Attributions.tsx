import React, { FC, useState } from 'react'

export const Attributions: FC = () => {
    let [show, setShow] = useState(false);

    const toggleView = () => {
        setShow(!show);
    };

    return (
        <div className="flex items-center justify-center flex-col mb-4 print:hidden">
            <span className="cursor-pointer text-blue-400" onClick={toggleView}>Attributions</span>

            {show &&
            <div className="bg-white p-2 rounded-md">
              <p>Background: <a href={'https://www.toptal.com/designers/subtlepatterns/email-pattern/'}>Email Pattern</a> by <a
                href={'https://tobyelliott.me/'}>Toby Elliott.</a> CC BY-SA 3.0 - Subtle Patterns © <a
                href={'https://www.toptal.com/designers'}>Toptal Designers.</a> / Slightly modified color compared to the original</p>
              <p>Icons - <a href={'https://iconmonstr.com/'}>iconmonstr</a>, <a href={'https://github.com/konpa/devicon/'}>konpa -
                devicons</a></p>
            </div>
            }
        </div>
    )
};
