import React, {FC} from 'react';
import {Footer} from "./Footer";
import {Outlet} from "react-router-dom";
import {Header} from "./Header";

export type GlobalLayoutProps = {}

export const GlobalLayout: FC<GlobalLayoutProps> = ({}) => {
    return (
        <>
            <Header/>
            <div className="mt-2 mb-8 mx-2 lg:mt-8 lg:mx-auto flex justify-center">
                <Outlet/>
            </div>
            <Footer className="mt-auto"/>
        </>
    )

};
