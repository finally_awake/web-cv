import React, { FC } from 'react';
import {Attributions} from "../misc/Attributions";

export type FooterProps = {
    className?: string
}

export const Footer: FC<FooterProps> = ({className}) => {
    return (
        <div className={className}>
            <Attributions/>
        </div>
    )
};
