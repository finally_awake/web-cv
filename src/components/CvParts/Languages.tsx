import React, { FC } from "react"
import { LanguageModel } from "../../misc/models";

export type LanguageProps = {
    languages: LanguageModel[]
}

export const Languages: FC<LanguageProps> = ({languages}) => {
    return (
        <>
            {/*<h2>Languages</h2>*/}

            <div className="flex flex-col gap-4">
                {languages.map(({name, proficiency}) =>
                    <div key={name}>
                        <p className="mb-2">{name} - {proficiency}</p>
                    </div>
                )}
            </div>

        </>
    )
};
