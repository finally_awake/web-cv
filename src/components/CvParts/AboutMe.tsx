import React, { FC } from 'react'

export type AboutMeProps = {
    text: string
}

export const AboutMe: FC<AboutMeProps> = ({text}) => {
    return (

        <div className="text-justify">
            {/*<h2>About me</h2>*/}
            <span>{text}</span>
        </div>
    )
};
