import React, { FC } from "react"
import { Institution } from "../../misc/models";

export type EducationProps = {
    institutions: Institution[]
}

export const Education: FC<EducationProps> = ({institutions}) => {
    return (
        <>
            {/*<h2>Education</h2>*/}

            <div className="flex flex-col gap-8">
                {institutions.map(({institutionName, degree, dateFrom, dateTo}) =>
                    <div key={degree}>
                        <p className="font-semibold mb-2">{institutionName}</p>
                        <p className="font-light mb-2">{degree}</p>
                        <p>{dateFrom} - {dateTo}</p>
                    </div>
                )}
            </div>
        </>
    )
};
