import React, { FC } from "react"

export type CandidateInfoFieldProps = {
    name: string,
    value: string | number
    logoPath: string
    link?: string
}

export const PersonalDataField: FC<CandidateInfoFieldProps> = ({name, value, logoPath, link}) => {
    return (
        <>
            <div className="flex">
                <img className="mr-4 h-[20px]" src={logoPath} alt={""}/>
                <div className="flex flex-col">
                    <p className="font-semibold mb-1">{name}:</p>
                    <a className={link && "text-blue-400"} href={link}>{value}</a>
                </div>
            </div>
        </>
    )
};
