import React, { FC } from "react"
import { PersonalDataModel } from "../../../misc/models";
import dayjs from "dayjs";
import { PersonalDataField } from "./PersonalDataField";
import { icnCalendar, icnEarth, icnLocation } from "../../../assets/icons/icons";

export type CandidateInfoProps = {
    personalData: PersonalDataModel
}

export const PersonalData: FC<CandidateInfoProps> = ({personalData}) => {
    const {location, birthDate, nationality} = personalData;
    let age = dayjs().diff(dayjs(birthDate), "year");

    return (
        <>
            {/*<h2>Personal Data</h2>*/}
            <div className="flex flex-col gap-6">
                <PersonalDataField name={"Location"} value={location} logoPath={icnLocation}/>
                <PersonalDataField name={"Age"} value={age} logoPath={icnCalendar}/>
                <PersonalDataField name={"Nationality"} value={nationality} logoPath={icnEarth}/>
            </div>
        </>
    )
};
