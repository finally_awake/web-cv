import React, { FC } from "react"
import { contactMap } from "../../../maps/contactIconMap";
import { PersonalDataModel } from "../../../misc/models";
import { PersonalDataField } from "./PersonalDataField";

export type ContactDataProps = {
    personalData: PersonalDataModel
}

export const ContactData: FC<ContactDataProps> = ({personalData}) => {
    const {contacts} = personalData;

    return (
        <>
            {/*<h2>Contacts</h2>*/}
            <div className="flex flex-col gap-6">
                {
                    contacts.map(contact =>
                        <PersonalDataField name={contact.name} value={contact.value} link={contact.link}
                                           logoPath={contactMap.get(contact.type)} key={contact.name}/>
                    )
                }
            </div>
        </>
    )
};
