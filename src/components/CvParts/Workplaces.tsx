import React, {FC} from "react"
import dayjs from "dayjs";
import "dayjs/locale/en"
import {getTimespanString} from "../../utils/utils";
import {WorkplaceModel} from "../../misc/models";
import {Markdown} from "../misc/Markdown";

export type WorkplaceProps = {
    workplaces: WorkplaceModel[]
}

export const Workplaces: FC<WorkplaceProps> = ({workplaces}) => {
    return (
        <>
            <h2>Work Experience</h2>
            <div className="flex flex-col gap-8 print:gap-4">
                {workplaces.map(({name, position, dateFrom, dateTo, responsibilities, location}) => {
                        const from = dayjs(dateFrom);
                        const to = dateTo === "Present" ? dayjs() : dayjs(dateTo);

                        return <div key={name}>
                            <p className="mb-0 text-lg font-semibold">{position}</p>
                            <p className="mb-0 text-sm font-light">{name} &bull; {location}</p>
                            <p className="mb-6 text-sm font-light">
                                <span>{from.format("MMMM")} {from.year()} - {to.format("MMMM")} {to.year()} ({getTimespanString(from, to)})</span>
                            </p>

                            <div className="mb-4">
                                <ul className="list-disc ml-5">
                                    {responsibilities.map(responsibility =>
                                        <li className="list-outside mb-1" key={responsibility}>
                                            <Markdown className="">{responsibility}</Markdown>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    }
                )}
            </div>
        </>
    )
};
