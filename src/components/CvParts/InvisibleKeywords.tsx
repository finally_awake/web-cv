import React, { FC } from 'react'

export type InvisibleKeywordsProps = {
    keywords: string[]
}

export const InvisibleKeywords: FC<InvisibleKeywordsProps> = ({keywords}) => {
    return (
        <div className="text-transparent text-[1px]">
            {keywords.map(word =>
                <span>{word} </span>
            )}
        </div>
    )
};
