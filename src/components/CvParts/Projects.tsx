import React, { FC } from "react"
import { ProjectModel } from "../../misc/models";
import {Markdown} from "../misc/Markdown";

export type ProjectProps = {
    projects: ProjectModel[]
    className?: string
}

export const Projects: FC<ProjectProps> = ({projects, className}) => {
    return (
        <div className={className}>
            <h2>Notable Projects</h2>

            <div className="flex flex-col gap-8">
                {projects.map(({name, longDescription, role, shortDescription, technologies}) =>
                    <div key={name}>
                        <div>
                                <p className="font-semibold text-lg mb-2">{name} - {shortDescription}</p>
                            <p className="">Role: {role}</p>
                            <p className="mb-4">Technologies: {technologies.join(", ")}</p>
                            <Markdown className="font-light">{longDescription}</Markdown>
                        </div>
                    </div>
                )}
            </div>


        </div>
    )
};
