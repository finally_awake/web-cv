import React, { FC } from "react"
import { SkillGroupModel } from "../../misc/models";

export type SkillsProps = {
    skillGroups: SkillGroupModel[]
}

export const Skills: FC<SkillsProps> = ({skillGroups}) => {
    return (
        <div>
            {/*<h2>Skills</h2>*/}
            <div className="flex flex-col gap-4">
                {skillGroups.map(group =>
                    <div key={group.name}>
                        <p className="mb-1 font-semibold">{group.name}: </p>
                        <ul className="list-disc ml-5">
                            {group.skills.map(skill =>
                                <li className="mb-1"> {skill}</li>
                            )}
                        </ul>
                    </div>
                )}
            </div>
        </div>
    )
};

