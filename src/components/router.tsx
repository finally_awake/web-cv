import {createBrowserRouter} from "react-router-dom";
import React from "react";
import {GlobalLayout} from "./layout/GlobalLayout";
import {CV} from "./CV";
import generalCv from "../CVs/general.json"
import strapiCv from "../CVs/strapi.json"

export const router = createBrowserRouter([
    {
        path: "/",
        element: <GlobalLayout/>,
        children: [
            {
                index: true,
                element: <CV cv={generalCv}/>,
            },
            {
                path: "/strapi",
                element: <CV cv={strapiCv}/>,
            },
        ]
    },

]);
