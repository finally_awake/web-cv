import React, { FC } from "react"
import { CVModel } from "../misc/models";
import { AboutMe } from "./CvParts/AboutMe";
import { Skills } from "./CvParts/Skills";
import { Workplaces } from "./CvParts/Workplaces";
import { PersonalData } from "./CvParts/personalData/PersonalData";
import { ContactData } from "./CvParts/personalData/ContactData";
import { Education } from "./CvParts/Education";
import { Languages } from "./CvParts/Languages";
import { Projects } from "./CvParts/Projects";
import { InvisibleKeywords } from "./CvParts/InvisibleKeywords";


export type CVProps = {
    cv: CVModel
}

export const CV: FC<CVProps> = ({cv}) => {
    return (
        <>
            <div className="max-w-5xl shadow-md border rounded-md bg-white print:shadow-none print:border-none print:max-w-full">
                {/*title*/}
                <div className="flex flex-wrap justify-center p-4 py-8">
                    <div className="text-center w-full">
                        <p className="text-3xl">{cv.personalData.name}</p>
                        <p className="text-xl font-bold">{cv.personalData.jobTitle}</p>
                    </div>

                    <div className="max-w-3xl m-auto">
                        <AboutMe text={cv.personalData.aboutMe}/>
                    </div>
                </div>

                <hr className="mx-8"/>

                <div className="flex flex-wrap grow-0 py-8">
                    {/*left column*/}
                    <div className="flex grow md:basis-1/5 flex-col px-4 lg:px-8 border-r">
                        <PersonalData personalData={cv.personalData}/>
                        <hr className="my-4"/>

                        <ContactData personalData={cv.personalData}/>
                        <hr className="my-4"/>

                        <Skills skillGroups={cv.skillGroups}/>
                        <hr className="my-4"/>

                        <Education institutions={cv.educations}/>
                        <hr className="my-4"/>

                        <Languages languages={cv.languages}/>
                        <hr className="my-4 mb-8 md:hidden"/>
                    </div>

                    {/*right column*/}
                    <div className="grow basis-3/5 px-4 lg:px-8">
                        <Workplaces workplaces={cv.workplaces}/>
                        <hr className="my-4 mb-8 print:hidden"/>

                        <Projects projects={cv.projects} className="break-before-page print:mt-8"/>

                        <InvisibleKeywords keywords={cv.keywords}/>
                    </div>


                </div>
            </div>
        </>
    )
};
