export type CVModel = {
    personalData: PersonalDataModel
    skillGroups: SkillGroupModel[]
    projects: ProjectModel[]
    workplaces: WorkplaceModel[]
    languages: LanguageModel[]
    educations: Institution[]
    keywords: string[]
}

export type PersonalDataModel = {
    name: string
    location: string
    birthDate: string
    nationality: string
    jobTitle: string
    aboutMe: string
    contacts: ContactModel[]
}

export type ContactModel = {
    type: string
    name: string
    value: string
    link: string
}
//

export type SkillGroupModel = {
    name: string
    skills: string[]
}
//
export type ProjectModel = {
    name: string
    shortDescription: string
    longDescription: string
    responsibilities: string[]
    role: string
    technologies: string[]
}
//
export type WorkplaceModel = {
    name: string
    location: string
    position: string
    dateFrom: string
    dateTo: string
    responsibilities: string[]
}
//
export type LanguageModel = {
    name: string
    proficiency: string
}
//
export type Institution = {
    institutionName: string
    degree: string
    dateFrom: string
    dateTo: string
}
